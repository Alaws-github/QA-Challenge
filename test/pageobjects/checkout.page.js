class CheckoutPage{

    get firstname () {return $('#first-name')}
    get lastname () {return $('#last-name')}
    get zipcode () {return $('#postal-code')}
    get continueBtn () {return $('#continue')}
    get finishBtn () {return $('#finish')}
    get thankyouMessage () {return $('.complete-header')}
    get backToHome () {return $('#back-to-products')}
    
    async continueCheckout(fname, lname, zipcode){
        await this.firstname.setValue(fname)
        await this.lastname.setValue(lname)
        await this.zipcode.setValue(zipcode)
        await this.continueBtn.click()
    }

    async clickOnFinishBtn(){
        await this.finishBtn.click()
    }
    
    async thankyouScreen(message){
        await expect(this.thankyouMessage).toHaveTextContaining(message)
        await this.backToHome.click()
    }
}

module.exports = new CheckoutPage();