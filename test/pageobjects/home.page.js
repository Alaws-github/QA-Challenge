
class HomePage {
    /**
     * define selectors using getter methods
     */
    get pageTitle () {return $('.title')}
    get itemTitle () {return $('inventory_item_name')}
    get itemDescription () {return $('inventory_item_desc')}
    get price () {return $('inventory_item_desc')}
    get addToCartBtn () {return $('#add-to-cart-sauce-labs-backpack')}
    get addToCartBtn2 () {return $('#add-to-cart-sauce-labs-bike-light')}
    get addToCartBtn3 () {return $('#add-to-cart-sauce-labs-bolt-t-shirt')}
    get removeFromCart () {return $('#remove-sauce-labs-backpack')}
    get cartCount () {return $('.shopping_cart_badge')}
    get menuButton () {return $('#react-burger-menu-btn')}
    get allItemButton() { return $('#inventory_sidebar_link')}
    get linkTitle () {return $('#item_4_title_link > div')}
    get twitter () {return $('=Twitter')}
    get facebook () {return $('=Facebook')}
    get linkedIn () {return $('=LinkedIn')}


    async verifyItems() {
        await this.itemTitle.isExisting()
        await this.itemDescription.isExisting()
        await this.price.isExisting()
        await this.addToCartBtn.isExisting()        
    }

    async navigateToHome() {
        //await this.allItemButton.click();
    }

    async verifyPageTitle(name) {
        await expect(this.pageTitle).toHaveTextContaining(name)
    }

    async addToCartItem(count){

        let elem = await this.cartCount
        let isExisting = await elem.isExisting()
        if(isExisting){
            await this.removeFromCart.click()
        }
            await this.addToCartBtn.click()
            await expect(this.cartCount).toHaveTextContaining(count) 
        
        
    }

    async addToCartMoreThanOneItem(count){
        await this.addToCartBtn.click()
        await this.addToCartBtn2.click()
        await this.addToCartBtn3.click()
        await expect(this.cartCount).toHaveTextContaining(count) 
        
        
    }

    async removeItemFromCart(name){
        await expect(this.removeFromCart).toHaveTextContaining(name)
        await this.removeFromCart.click()
    }

    async checkItemTitleClickable(name){
        await this.linkTitle.isClickable()
        await this.linkTitle.click()
    }

    async footerTwitterLink(flink){
        const link = await this.twitter
        await expect(link).toHaveText('Twitter')
        await expect(link).toHaveAttribute('href', flink)
    }

    async footerFacebookLink(flink){
        const link = await this.facebook
        await expect(link).toHaveText('Facebook')
        await expect(link).toHaveAttribute('href', flink)
    }

    async footerLinkedInLink(flink){
        const link = await this.linkedIn
        await expect(link).toHaveText('LinkedIn')
        await expect(link).toHaveAttribute('href', flink)
    }

    async navigateToCart() {
        await this.cartCount.click()
    }

    // async checkProductsAreDisplayed() {
    //     await expect(this.productMenu).toBeDisplayed();
    // }


}

module.exports = new HomePage();
