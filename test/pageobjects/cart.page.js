class CartPage{
    get continueShoppingBtn () {return $('#continue-shopping')}
    get checkoutBtn () {return $('#checkout')}
    get itemName () {return $('.inventory_item_name')} 


    async btnExist(){
        await this.continueShoppingBtn.isExisting()
        await this.checkoutBtn.isExisting()
    }

    async itemExist(name){
        await expect(this.itemName).toHaveTextContaining(name)
    }

    async clickOnCheckoutBtn(){
        await this.checkoutBtn.click()
    }
}

module.exports = new CartPage();