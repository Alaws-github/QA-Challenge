const LoginPage = require('../pageobjects/login.page')
const HomePage = require('../pageobjects/home.page')
const SecurePage = require('../pageobjects/secure.page')
const userData = require('../../data/user.data')
const CartPage = require('../pageobjects/cart.page')
const CheckoutPage = require('../pageobjects/checkout.page')


describe('My Inventory Page', () => {

    before('Should log in with valid credentials', async () => {
    browser.url('/');
    browser.maximizeWindow()
    await LoginPage.login(userData.correctCredentials.username, userData.correctCredentials.password)

});

    //Case 01
    it('should verify product listing exist title, description, price and add to cart button', async () => {
        await HomePage.verifyPageTitle('Products')
        await HomePage.verifyItems()
    })

        //Case 02
        it('should add a item to cart and verify button text change', async () => {
            await HomePage.addToCartItem(1)
            await HomePage.removeItemFromCart('Remove')
        })

        //Case 03
        it('should verify title of an item is clickable', async () => {
            await HomePage.checkItemTitleClickable()
        })

        //Case 04
        it('should verify with problem user', async () => {
            await LoginPage.logout() 
            await LoginPage.login(userData.problemUser.username, userData.problemUser.password)
            await HomePage.addToCartItem(1)
            await HomePage.removeItemFromCart('Remove')
        })

        //Case 05
        it('should verify the tTwitter footer links', async () => {
            await HomePage.footerTwitterLink('https://twitter.com/saucelabs')
        })

        //Case 06
        it('should verify the Facebook footer links', async () => {
            await HomePage.footerFacebookLink('https://www.facebook.com/saucelabs')
        })
        //Case 07
        it('should verify the LinkedIn footer links', async () => {
            await HomePage.footerLinkedInLink('https://www.linkedin.com/company/sauce-labs/')
            await LoginPage.logout() 
        })
})
