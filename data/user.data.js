module.exports = {
    correctCredentials: {
        username: 'standard_user',
        password: 'secret_sauce'
    },
    incorrectPassword: {
        username: 'desk@desk.com',
        password: '1desk'
    },
    noPassword: {
        username: 'desk@desk.com',
        password: ''
    },
    
    invalidEMail: {
        username: 'deskter@desk.com',
        password: 'secret_sauce'
    },

    lockedOutUser: {
        username: 'locked_out_user',
        password: 'secret_sauce'
    },

    problemUser: {
        username: 'problem_user',
        password: 'secret_sauce'
    },

    checkoutDetails: {
        firstname: 'Tester',
        lastname: 'Test',
        zipcode: '12345'
    }
};